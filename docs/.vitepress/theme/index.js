import DefaultTheme from 'vitepress/theme'
import '~/styles/ellipsis.scss'
import components from '~/index.js'

export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    app.use(components)
  },
}
