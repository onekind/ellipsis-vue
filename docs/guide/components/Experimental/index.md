# Advanced

For these animations to be correct, you need to apply the following styling to the container.

```css
filter: contrast(5);
background-color: white;
```

## gathering

```vue
<ok-ellipsis flavour="gathering" />
```

<Sample title="gathering" contrast>
  <ok-ellipsis flavour="gathering" />
</Sample>

## hourglass

```vue
<ok-ellipsis flavour="hourglass" />
```

<Sample title="hourglass" contrast>
  <ok-ellipsis flavour="hourglass" />
</Sample>

## overtaking

```vue
<ok-ellipsis flavour="overtaking" />
```

<Sample title="overtaking" contrast>
  <ok-ellipsis flavour="overtaking" />
</Sample>

## shuttle

```vue
<ok-ellipsis flavour="shuttle" />
```

<Sample title="shuttle" contrast>
  <ok-ellipsis flavour="shuttle" />
</Sample>

## bouncing

```vue
<ok-ellipsis flavour="bouncing" />
```

<Sample title="bouncing">
  <ok-ellipsis flavour="bouncing" />
</Sample>

## rolling

```vue
<ok-ellipsis flavour="rolling" />
```

<Sample title="rolling">
  <ok-ellipsis flavour="rolling" />
</Sample>

<script setup>
import Sample from '../../../Components/Sample.vue'
</script>
