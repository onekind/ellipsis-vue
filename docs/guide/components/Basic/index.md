# Ellipsis

## elastic

```vue
<ok-ellipsis flavour="elastic" />
```

<Sample title="elastic">
  <ok-ellipsis flavour="elastic" />
</Sample>

## pulse

```vue
<ok-ellipsis flavour="pulse" />
```

<Sample title="pulse">
  <ok-ellipsis flavour="pulse"></ok-ellipsis>
</Sample>

## flashing

```vue
<ok-ellipsis flavour="flashing" />
```

<Sample title="flashing">
  <ok-ellipsis flavour="flashing"></ok-ellipsis>
</Sample>

## collision

```vue
<ok-ellipsis flavour="collision" />
```

<Sample title="collision">
  <ok-ellipsis flavour="collision"></ok-ellipsis>
</Sample>

## revolution

```vue
<ok-ellipsis flavour="revolution" />
```

<Sample title="revolution">
  <ok-ellipsis flavour="revolution"></ok-ellipsis>
</Sample>

## carousel

```vue
<ok-ellipsis flavour="carousel" />
```

<Sample title="carousel">
  <ok-ellipsis flavour="carousel"></ok-ellipsis>
</Sample>

## typing

```vue
<ok-ellipsis flavour="typing" />
```

<Sample title="typing">
  <ok-ellipsis flavour="typing"></ok-ellipsis>
</Sample>

## windmill

```vue
<ok-ellipsis flavour="windmill" />
```

<Sample title="windmill">
  <ok-ellipsis flavour="windmill"></ok-ellipsis>
</Sample>

## bricks

```vue
<ok-ellipsis flavour="bricks" />
```

<Sample title="bricks">
  <ok-ellipsis flavour="bricks"></ok-ellipsis>
</Sample>

## floating

```vue
<ok-ellipsis flavour="floating" />
```

<Sample title="floating">
  <ok-ellipsis flavour="floating"></ok-ellipsis>
</Sample>

## fire

```vue
<ok-ellipsis flavour="fire" />
```

<Sample title="fire">
  <ok-ellipsis flavour="fire"></ok-ellipsis>
</Sample>

## spin

```vue
<ok-ellipsis flavour="spin" />
```

<Sample title="spin">
  <ok-ellipsis flavour="spin"></ok-ellipsis>
</Sample>

## falling

```vue
<ok-ellipsis flavour="falling" />
```

<Sample title="falling">
  <ok-ellipsis flavour="falling"></ok-ellipsis>
</Sample>

## stretching

```vue
<ok-ellipsis flavour="stretching" />
```

<Sample title="stretching">
  <ok-ellipsis flavour="stretching"></ok-ellipsis>
</Sample>

<script setup>
import Sample from '../../../Components/Sample.vue'
</script>
