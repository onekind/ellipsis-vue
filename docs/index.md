---
title: Introduction
---

Inspired by the work from [nzbin](https://nzbin.github.io/three-dots/). Ellipsis are a set of vue3 composition API dots with various animations, ready for any project which requires such feature.

## Instalation

```bash
yarn add @onekind/ellipsis-vue
```

## Setting up

- Add the following to you application main.js file:

```js
import {OkEllipsis} from '@onekind/ellipsis-vue'
import '@onekind/ellipsis-vue/dist/ellipsis-vue.css'
app.component(OkEllipsis.name, OkEllipsis)
```

## Customizing

You may use the `scss` files instead, which offer more customization.

- **color**: Optionally setup the color, before you import the main `scss` file.

```scss
$circle-color: rgb(0,0,255);
```

- **diameter**: Optionally setup the circle diameter, before you import the main `scss` file.

```scss
$circle-diameter: 8px;
```

- Import the main style `scss` file.

```scss
@import '@onekind/ellipsis-vue/src/styles/ellipsis.scss';
```

## Using

```vue
<ok-ellipsis />
```

<Testbed flavour="elastic"></Testbed>

<script setup>
import Testbed from './Components/Testbed.vue'
</script>
